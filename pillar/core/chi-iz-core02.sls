eth0:
  address: 192.168.8.82/24
  gateway: 192.168.8.1

vrfs:
  - ATRND_IZ
  - AR_SQL
  - STD_IZ
  - AM_SQL
  - CADB_SQL
  - OUTB_SQL
  - IBR_SQL
  - STD_SQL
  - NFT_SQL
  - MGMT_IZ

vlans:
  - vlan1000:
      description: ATTRND_GRP
      vlanid: 1000
      vrf: ATRND_IZ
      address: 10.2.0.3/23
      vrr:
        ip: 10.2.0.1/23
        mac: 00:00:5e:00:01:01

  - vlan1001:
      description: FAXLYNC_GRP
      vlanid: 1001
      vrf: STD_IZ
      address: 10.2.2.3/24
      vrr:
        ip: 10.2.2.1/24
        mac: 00:00:5e:00:01:01

  - vlan1002:
      description: INVOICE_GRP
      vlanid: 1002
      vrf: STD_IZ
      address: 10.2.3.3/24
      vrr:
        ip: 10.2.3.1/24
        mac: 00:00:5e:00:01:01

  - vlan1003:
      description: IWSFILE_GRP
      vlanid: 1003
      vrf: STD_IZ
      address: 10.2.4.3/24
      vrr:
        ip: 10.2.4.1/24
        mac: 00:00:5e:00:01:01

  - vlan1004:
      description: JM_GRP
      vlanid: 1004
      vrf: STD_IZ
      address: 10.2.5.3/24
      vrr:
        ip: 10.2.5.1/24
        mac: 00:00:5e:00:01:01

  - vlan1005:
      description: LDAP_GRP
      vlanid: 1005
      vrf: STD_IZ
      address: 10.2.6.3/24
      vrr:
        ip: 10.2.6.1/24
        mac: 00:00:5e:00:01:01

  - vlan1006:
      description: LOOPFAX_GRP
      vlanid: 1006
      vrf: STD_IZ
      address: 10.2.7.3/24
      vrr:
        ip: 10.2.7.1/24
        mac: 00:00:5e:00:01:01

  - vlan1007:
      description: NETMON_GRP
      vlanid: 1007
      vrf: STD_IZ
      address: 10.2.8.3/24
      vrr:
        ip: 10.2.8.1/24
        mac: 00:00:5e:00:01:01

  - vlan1008:
      description: NFT_GRP
      vlanid: 1008
      vrf: STD_IZ
      address: 10.2.9.3/24
      vrr:
        ip: 10.2.9.1/24
        mac: 00:00:5e:00:01:01

  - vlan1009:
      description: ORDER_ENTRY_GRP
      vlanid: 1009
      vrf: STD_IZ
      address: 10.2.10.3/24
      vrr:
        ip: 10.2.10.1/24
        mac: 00:00:5e:00:01:01

  - vlan1010:
      description: PPR_GRP
      vlanid: 1010
      vrf: STD_IZ
      address: 10.2.11.3/24
      vrr:
        ip: 10.2.11.1/24
        mac: 00:00:5e:00:01:01

  - vlan1011:
      description: PROV_GRP
      vlanid: 1011
      vrf: STD_IZ
      address: 10.2.12.3/24
      vrr:
        ip: 10.2.12.1/24
        mac: 00:00:5e:00:01:01

  - vlan1012:
      description: SIP_OFS_GRP
      vlanid: 1012
      vrf: STD_IZ
      address: 10.2.13.3/24
      vrr:
        ip: 10.2.13.1/24
        mac: 00:00:5e:00:01:01

  - vlan1013:
      description: SMTPGW_IZ_GRP
      vlanid: 1013
      vrf: STD_IZ
      address: 10.2.14.3/24
      vrr:
        ip: 10.2.14.1/24
        mac: 00:00:5e:00:01:01

  - vlan1014:
      description: TDM_IFS_GRP
      vlanid: 1014
      vrf: STD_IZ
      address: 10.2.15.3/24
      vrr:
        ip: 10.2.15.1/24
        mac: 00:00:5e:00:01:01

  - vlan1015:
      description: TDM_OFS_GRP
      vlanid: 1015
      vrf: STD_IZ
      address: 10.2.16.3/24
      vrr:
        ip: 10.2.16.1/24
        mac: 00:00:5e:00:01:01

  - vlan1016:
      description: ACCTMGMT_SQL_GRP
      vlanid: 1016
      vrf: AM_SQL
      address: 10.2.17.3/24
      vrr:
        ip: 10.2.17.1/24
        mac: 00:00:5e:00:01:01

  - vlan1017:
      description: ARCHIVE_SQL_GRP
      vlanid: 1017
      vrf: AR_SQL
      address: 10.2.18.3/24
      vrr:
        ip: 10.2.18.1/24
        mac: 00:00:5e:00:01:01

  - vlan1018:
      description: CADB_SQL_GRP
      vlanid: 1018
      vrf: CADB_SQL
      address: 10.2.19.3/24
      vrr:
        ip: 10.2.19.1/24
        mac: 00:00:5e:00:01:01

  - vlan1020:
      description: FDPWORKING_GRP
      vlanid: 1020
      vrf: OUTB_SQL
      address: 10.2.21.3/24
      vrr:
        ip: 10.2.21.1/24
        mac: 00:00:5e:00:01:01

  - vlan1021:
      description: IBR_SQL_GRP
      vlanid: 1021
      vrf: IBR_SQL
      address: 10.2.22.3/24
      vrr:
        ip: 10.2.22.1/24
        mac: 00:00:5e:00:01:01

  - vlan1022:
      description: IDENTITY_SQL_GRP
      vlanid: 1022
      vrf: STD_SQL
      address: 10.2.23.3/24
      vrr:
        ip: 10.2.23.1/24
        mac: 00:00:5e:00:01:01

  - vlan1023:
      description: JT_SQL_GRP
      vlanid: 1023
      vrf: STD_SQL
      address: 10.2.24.3/24
      vrr:
        ip: 10.2.24.1/24
        mac: 00:00:5e:00:01:01

  - vlan1024:
      description: KB_SQL_GRP
      vlanid: 1024
      vrf: STD_SQL
      address: 10.2.25.3/24
      vrr:
        ip: 10.2.25.1/24
        mac: 00:00:5e:00:01:01

  - vlan1025:
      description: LOG4NET_GRP
      vlanid: 1025
      vrf: STD_SQL
      address: 10.2.26.3/24
      vrr:
        ip: 10.2.26.1/24
        mac: 00:00:5e:00:01:01

  - vlan1026:
      description: MD_SQL_GRP
      vlanid: 1026
      vrf: STD_SQL
      address: 10.2.27.3/24
      vrr:
        ip: 10.2.27.1/24
        mac: 00:00:5e:00:01:01

  - vlan1027:
      description: NFT_SQL_GRP
      vlanid: 1027
      vrf: NFT_SQL
      address: 10.2.28.3/24
      vrr:
        ip: 10.2.28.1/24
        mac: 00:00:5e:00:01:01

  - vlan1028:
      description: SLA_SQL_GRP
      vlanid: 1028
      vrf: STD_SQL
      address: 10.2.29.3/24
      vrr:
        ip: 10.2.29.1/24
        mac: 00:00:5e:00:01:01

  - vlan1029:
      description: SQL_FILE_GRP
      vlanid: 1029
      vrf: STD_SQL
      address: 10.2.30.3/24
      vrr:
        ip: 10.2.30.1/24
        mac: 00:00:5e:00:01:01

  - vlan1030:
      description: MGMT_IZ
      vlanid: 1030
      vrf: MGMT_IZ
      address: 10.2.31.3/24
      vrr:
        ip: 10.2.31.1/24
        mac: 00:00:5e:00:01:01