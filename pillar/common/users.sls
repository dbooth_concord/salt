users:
  default_user:
    name: operations
    gid: 1001
    uid: 1001
    shell: /bin/bash
    home: /home/operations
    password: $6$SALTsalt$0wF/lsvqwjriWxU4RWnYbzTh17cnPGmDQ2j3BLcEQruqiKWJw2JK4isfz8duDpKwFdSEGRxPwklzRFruGoALq1
    enforce_password: True
    groups:
      - operations
      - sudo