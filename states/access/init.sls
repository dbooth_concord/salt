/etc/network/interfaces:
  file.managed:
    - name: /etc/network/interfaces
    - source: salt://access/interfaces.j2
    - template: jinja
    - show_diff: True
    - defaults:
        eth0: {{ salt['pillar.get']('eth0') }}
        vlans: {{ salt['pillar.get']('vlans', {}) }}