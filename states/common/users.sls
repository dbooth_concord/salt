{% for user, args in pillar['users'].iteritems() %}
{{ user }}:
  user.present:
    - name: {{ args['name'] }}
    - home: {{ args['home'] }}
    - shell: {{ args['shell'] }}
{% if 'password' in args %}
    - password: {{ args['password'] }}
{% endif %}
{% if 'enforce_password' in args %}
    - enforce_password: {{ args['enforce_password'] }}
{% endif %}
{% if 'groups' in args %}
    - groups: {{ args['groups'] }}
{% endif %}
{% endfor %}