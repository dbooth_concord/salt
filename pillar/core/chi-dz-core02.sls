eth0:
  address: 192.168.8.84/24
  gateway: 192.168.8.1

vlans:
- vlan2000:
    description: BYON_IFS_GRP
    vlanid: 2000

- vlan2001:
    description: FBSUBMIT_GRP
    vlanid: 2001

- vlan2002:
    description: FFAX_GRP
    vlanid: 2002

- vlan2003:
    description: FTP_STORAGE_GRP
    vlanid: 2003

- vlan2004:
    description: FTP_SUB_GRP
    vlanid: 2004

- vlan2005:
    description: INBD_FWD_GRP
    vlanid: 2005

- vlan2006:
    description: INTRANET_GRP
    vlanid: 2006

- vlan2007:
    description: LRPC_GRP
    vlanid: 2007

- vlan2008:
    description: MAILOB_GRP
    vlanid: 2008

- vlan2009:
    description: SIGNUP_WEB_GRP
    vlanid: 2009

- vlan2010:
    description: SIP_IFS_GRP
    vlanid: 2010

- vlan2011:
    description: SMTPGW_DZ_GRP
    vlanid: 2011

- vlan2012:
    description: VB_IFS_GRP
    vlanid: 2012

- vlan2013:
    description: WEBPORTAL_GRP
    vlanid: 2013

- vlan2014:
    description: WSGW_GRP
    vlanid: 2014

- vlan2015:
    description: MGMT_DZ
    vlanid: 2015

- vlan2016:
    description: TEMP
    vlanid: 2016