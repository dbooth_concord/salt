#!/bin/bash
function error() {
  echo -e "\e[0;33mERROR: The Zero Touch Provisioning script failed while running the command $BASH_COMMAND at line $BASH_LINENO.\e[0m" >&2
  exit 1
}

# Log all output from this script
exec >> /var/log/autoprovision 2>&1
date "+%FT%T ztp starting script $0"


trap error ERR

#Add Debian Repositories
echo "deb http://http.us.debian.org/debian jessie main" >> /etc/apt/sources.list
echo "deb http://security.debian.org/ jessie/updates main" >> /etc/apt/sources.list
echo "deb http://ftp.us.debian.org/debian/ jessie main contrib non-free" >> /etc/apt/sources.list


# Waiting for NCLU to finish starting up
last_code=1
while [ "1" == "$last_code" ]; do
    net show interface &> /dev/null
    last_code=$?
done

#Update time and nameservers
net add time zone America/Los_Angeles
net add dns nameserver 8.8.8.8
net del dns nameserver 192.168.8.16
net del dns nameserver 192.168.8.17
net commit

#Determine host from serial number
serial_number=$(dmidecode -t system | grep Serial | cut -d ' ' -f 3)

device_details=`python3 << END
import os

data = [('581254X1908017', 'CHI-IZ-SW01', '192.168.8.226'), ('581254X1908018', 'CHI-IZ-SW02', '192.168.8.227'), ('581254X1908019', 'CHI-IZ-SW03', '192.168.8.228'), ('581254X1908021', 'CHI-IZ-SW04', '192.168.8.229'), ('581254X1908024', 'CHI-IZ-SW05', '192.168.8.230'), ('581254X1908025', 'CHI-IZ-SW06', '192.168.8.231'), ('581254X1908030', 'CHI-IZ-SW07', '192.168.8.232'), ('581254X1908031', 'CHI-IZ-SW08', '192.168.8.233'), ('581254X1908032', 'CHI-IZ-SW09', '192.168.8.234'), ('581254X1908035', 'CHI-IZ-SW10', '192.168.8.235'), ('581254X1908046', 'CHI-IZ-SW11', '192.168.8.236'), ('581254X1908047', 'CHI-IZ-SW12', '192.168.8.237'), ('581254X1908048', 'CHI-IZ-SW13', '192.168.8.238'), ('581254X1908049', 'CHI-IZ-SW14', '192.168.8.239')]

local_serial = os.environ.get('serial_number')

for device in data:
    serial = device[0]
    hostname = device[1]
    ip = device[2]


    if serial == local_serial:
        print('net add hostname ' + (hostname))
        #print('net add interface eth0 ip address ' + (ip)+'/24')
END`

$device_details


#Install License
license="jfleming@concordfax.com|4Em3MCACDgPjsXlCLtLNhXUjX1q9Ag4MiQiU7TNhURuPqmt+Ww"
function install_license(){
    echo "$(date) INFO: Installing License..."
    echo $1 | /usr/cumulus/bin/cl-license -i
    return_code=$?
    if [ "$return_code" == "0" ]; then
        echo "$(date) INFO: License Installed."
    else
        echo "$(date) ERROR: License not installed. Return code was: $return_code"
        /usr/cumulus/bin/cl-license
        exit 1
    fi
}

install_license $license

#Update Package Cache
apt-get update -y

#Install Salt
curl -L https://bootstrap.saltstack.com -o install_salt.sh
sh install_salt.sh -A 192.168.7.101

# #Reload interfaces to apply loaded config
ifreload -a

# CUMULUS-AUTOPROVISIONING
exit 0
