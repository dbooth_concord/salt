/etc/snmp/snmpd.conf:
  file.append:
    - text: |
        agentaddress {{ grains.ip4_interfaces.eth0[0] }}
        rocommunity c3##read default