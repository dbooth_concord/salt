/etc/network/interfaces:
  file.managed:
    - name: /etc/network/interfaces
    - source: salt://core/interfaces.j2
    - template: jinja
    - show_diff: True
    - defaults:
        eth0: {{ salt['pillar.get']('eth0') }}
        vrfs: {{ salt['pillar.get']('vrfs', {}) }}
        vlans: {{ salt['pillar.get']('vlans', {}) }}