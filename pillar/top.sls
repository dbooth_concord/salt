base:
  '*':
    - common.users
    - common.groups
  CHI-IZ-CORE01:
    - core.chi-iz-core01
  CHI-IZ-CORE02:
    - core.chi-iz-core02
  CHI-DZ-CORE01:
    - core.chi-dz-core01
  CHI-DZ-CORE02:
    - core.chi-dz-core02
  CHI-IZ-AC01:
    - access.chi-iz-ac01